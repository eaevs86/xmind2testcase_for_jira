#!/usr/bin/env python
# _*_ coding:utf-8 _*_
import csv
import logging
import os
from xmind2testcase.utils import get_xmind_testcase_list, get_absolute_path

"""
Convert XMind fie to Zentao testcase csv file 

Zentao official document about import CSV testcase file: https://www.zentao.net/book/zentaopmshelp/243.mhtml 
"""


def xmind_to_zentao_csv_file(xmind_file):

    """Convert XMind file to a zentao csv file"""
    xmind_file = get_absolute_path(xmind_file)
    logging.info('Start converting XMind file(%s) to zentao file...', xmind_file)
    testcases = get_xmind_testcase_list(xmind_file)

    fileheader =["优先级",  "模块", "标题", "描述（前置条件）","经办人", "报告人", "需求", "步骤ID","步骤",'测试数据','期望结果','测试用例集','用例标签']
        # ["所属模块", "用例标题", "前置条件", "步骤", "预期", "关键词", "优先级", "用例类型", "适用阶段"]
    zentao_testcase_rows = [fileheader]
    for testcase in testcases:
        row = gen_a_testcase_row(testcase)

        zentao_testcase_rows.append(row)


    A = [['优先级', '模块', '标题', '描述（前置条件）', '经办人', '报告人', '需求', '步骤ID', '步骤', '测试数据', '期望结果','测试用例集','用例标签']]
    for i in range(1,len(zentao_testcase_rows)):

        #防止测试结果为空导致数组越界
        if zentao_testcase_rows[i][8] != '' and zentao_testcase_rows[i][10]== '':
            a = zentao_testcase_rows[i][7].strip(', ').split(',')
            b = zentao_testcase_rows[i][8].strip(', ').split(',')
            if len(b) == 1:
                C = [zentao_testcase_rows[i][0], zentao_testcase_rows[i][1], zentao_testcase_rows[i][2],
                     zentao_testcase_rows[i][3], zentao_testcase_rows[i][4], zentao_testcase_rows[i][5],
                     zentao_testcase_rows[i][6], a[0], b[0], zentao_testcase_rows[i][9], '',zentao_testcase_rows[i][11],zentao_testcase_rows[i][12]]
                A.append(C)

            else:
                for j in range(len(b)):
                    if j == 0:
                        C = [zentao_testcase_rows[i][0], zentao_testcase_rows[i][1], zentao_testcase_rows[i][2],
                             zentao_testcase_rows[i][3], zentao_testcase_rows[i][4], zentao_testcase_rows[i][5],
                             zentao_testcase_rows[i][6], a[0], b[0], zentao_testcase_rows[i][9], '',zentao_testcase_rows[i][11],zentao_testcase_rows[i][12]]
                        A.append(C)
                    else:

                        C = ['', '', '', '', zentao_testcase_rows[i][4], zentao_testcase_rows[i][5],
                             zentao_testcase_rows[i][6], a[j], b[j], zentao_testcase_rows[i][9], '',zentao_testcase_rows[i][11],zentao_testcase_rows[i][12]]
                        A.append(C)

        elif zentao_testcase_rows[i][8] == '' and zentao_testcase_rows[i][10]== '':
            C = [zentao_testcase_rows[i][0], zentao_testcase_rows[i][1], zentao_testcase_rows[i][2],
                 zentao_testcase_rows[i][3], zentao_testcase_rows[i][4], zentao_testcase_rows[i][5],
                 zentao_testcase_rows[i][6], '', '', zentao_testcase_rows[i][9], '',zentao_testcase_rows[i][11],zentao_testcase_rows[i][12]]
            A.append(C)

        else:
            a = zentao_testcase_rows[i][7].strip(', ').split(',')
            b = zentao_testcase_rows[i][8].strip(', ').split(',')
            c = zentao_testcase_rows[i][10].strip(', ').split(',')

            ##xmind有的步骤没有测试结果，补齐为''
            X = []
            for index_result in c:
                result = index_result.strip('. ').split('.')
                for index_num in result:
                    X.append(index_num)

            for z in range(1, len(b) + 1):

                if str(z) in X:
                    pass
                else:
                    c.insert(z - 1, '')


            if len(b)==1:
                C = [zentao_testcase_rows[i][0], zentao_testcase_rows[i][1], zentao_testcase_rows[i][2],
                     zentao_testcase_rows[i][3], zentao_testcase_rows[i][4], zentao_testcase_rows[i][5],
                     zentao_testcase_rows[i][6], a[0], b[0], zentao_testcase_rows[i][9], c[0],zentao_testcase_rows[i][11],zentao_testcase_rows[i][12]]
                A.append(C)

            else:
                for j in range(len(b)):
                    if j==0:
                        C = [zentao_testcase_rows[i][0], zentao_testcase_rows[i][1], zentao_testcase_rows[i][2],
                             zentao_testcase_rows[i][3], zentao_testcase_rows[i][4], zentao_testcase_rows[i][5],
                             zentao_testcase_rows[i][6], a[0], b[0], zentao_testcase_rows[i][9], c[0],zentao_testcase_rows[i][11],zentao_testcase_rows[i][12]]
                        A.append(C)
                    else:
                        if j<=len(c)-1:

                            C = ['', '', '', '', zentao_testcase_rows[i][4], zentao_testcase_rows[i][5],
                                 zentao_testcase_rows[i][6], a[j], b[j], zentao_testcase_rows[i][9], c[j],zentao_testcase_rows[i][11],zentao_testcase_rows[i][12]]
                            A.append(C)
                        else:
                            C = ['', '', '', '', zentao_testcase_rows[i][4], zentao_testcase_rows[i][5],
                                 zentao_testcase_rows[i][6], a[j], b[j], zentao_testcase_rows[i][9], '',zentao_testcase_rows[i][11],zentao_testcase_rows[i][12]]
                            A.append(C)




    zentao_file = xmind_file[:-6] + '.csv'
    if os.path.exists(zentao_file):
        os.remove(zentao_file)
        # logging.info('The zentao csv file already exists, return it directly: %s', zentao_file)
        # return zentao_file

    with open(zentao_file, 'w', encoding='utf8', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(A)
        logging.info('Convert XMind file(%s) to a zentao csv file(%s) successfully!', xmind_file, zentao_file)

    return zentao_file


def  gen_a_testcase_row(testcase_dict):
    case_module = gen_case_module(testcase_dict['suite'])


    #替换掉最后一个'/'
    case_title = testcase_dict['name']
    string = list(case_title)
    string[len(case_title) - 1] = ''
    case_title=''.join(string).replace(' ','')

    case_precontion_all = testcase_dict['preconditions'].replace('/','')
    case_precontion1=case_precontion_all.split('\\n')[0].split('----')
    if len(case_precontion1)>0:
        case_precontion=case_precontion1[-1].replace('\n','')
        test_case_lable=case_precontion1[0].replace('\n','')
    elif len(case_precontion1)==0:
        case_precontion=case_precontion1[0].replace('\n','')
        test_case_lable=''


    case_expected_result = gen_case_step_and_expected_result(testcase_dict['steps'])
    case_step = gen_case_step_and_expected_step(testcase_dict['steps'])

    case_num = gen_case_step_and_expected_num(testcase_dict['steps'])

    case_keyword = ''

    #case_type = gen_case_type(testcase_dict['execution_type'])
    # case_type = gen_case_type(testcase_dict['summary'])
    #case_apply_phase = '迭代测试'
    case_apply_phase_all = gen_case_apply_phase(testcase_dict['summary']).replace('/','')

    case_apply_phase1=case_apply_phase_all.split('\\n')[0].split('----')
    if len(case_apply_phase1)>0:
        case_apply_phase=case_apply_phase1[-1].replace('\n','')
        test_case_leve=case_apply_phase1[0].replace('\n','')
    elif len(case_apply_phase1)==0:
        case_apply_phase=case_apply_phase1[0].replace('\n','')
        test_case_leve=''

    case_needs=gen_case_needs(testcase_dict['execution_type'])

    case_priority = gen_case_priority(testcase_dict['importance'])

    row = [case_priority, case_module, case_title, case_precontion, '', '', case_needs, case_num, case_step,
               case_apply_phase, case_expected_result,test_case_lable,test_case_leve]
        # [case_module, case_title, case_precontion, case_step, case_expected_result, case_keyword, case_priority, case_type, case_apply_phase]
    return row


def gen_case_module(module_name):
    if module_name:
        module_name = module_name.replace('（', '(')
        module_name = module_name.replace('）', ')')
    else:
        module_name = '/'
    return module_name



# def gen_case_step_and_expected_step(steps):
#     case_step = ''
#     case_expected_result = ''
#
#     for step_dict in steps:
#         case_step += str(step_dict['step_number']) + '. ' + step_dict['actions'].replace('\n', '').strip() + '\n'
#         case_expected_result += str(step_dict['step_number']) + '. ' + \
#             step_dict['expectedresults'].replace('\n', '').strip() + '\n' \
#             if step_dict.get('expectedresults', '') else ''
#
#     return case_step, case_expected_result

def gen_case_needs(case_needs):
    for case_need in case_needs:

        return case_need


def gen_case_step_and_expected_result(steps):

    case_expected_result = ''

    for step_dict in steps:
        #替换掉Xmind源文件中的换行、回车、空格和英文逗号
        case_expected_result += str(step_dict['step_number']) + '. ' + \
            step_dict['expectedresults'].replace('\n', '').replace('\r', '').replace(',', '，').replace(' ', '').strip() + '\n' +','\
            if step_dict.get('expectedresults', '') else ''

    return case_expected_result

def gen_case_step_and_expected_step(steps):
    case_step = ''

    # 替换掉Xmind源文件中的换行、回车、空格和英文逗号
    for step_dict in steps:
        case_step += str(step_dict['step_number'])+  '. ' +step_dict['actions'].replace('\n', '').replace(' ', '').replace(',', '，').replace('\r', '').strip() + '\n'+','


    return case_step
def gen_case_step_and_expected_num(steps):
    case_num =''


    for step_dict in steps:
        case_num +=(str(step_dict['step_number']).replace('\n', '').replace(' ', '').strip())+','


    return case_num


def gen_case_priority(priority):
    mapping = {1: 'Highest', 2: 'High', 3: 'Medium',4: 'Low',5: 'Lowest'}
    # mapping = {1: 1, 2: 2, 3: 3}
    if priority in mapping.keys():
        return mapping[priority]
    else:
        return 'High'


def gen_case_type(case_type):
    if case_type=='无':
        return ''
    else:
        return case_type
def gen_case_apply_phase(case_apply_phase):
    if case_apply_phase=='无':
        return ''
    else:
        return case_apply_phase

# def gen_case_apply_banben(case_apply_banben):
#     if case_apply_banben=='无':
#         return case_apply_banben
#     else:
#         return case_apply_banben

if __name__ == '__main__':
    xmind_file = '../docs/zentao_testcase_template.xmind'
    zentao_csv_file = xmind_to_zentao_csv_file(xmind_file)
    print('Conver the xmind file to a zentao csv file succssfully: %s', zentao_csv_file)