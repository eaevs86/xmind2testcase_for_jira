

from flask import Flask, request, render_template

app=Flask(__name__)


@app.route("/1", methods=["GET", "POST"])
def request_test():
    request_body_header = request.headers

    if request_body_header.get('Content-Type')=='application/json':
        request_body = request.data
        return request_body
    else:
        request_body_formdata=request.values
        return request_body_formdata

@app.route("/2", methods=["GET", "POST"])
def hello_world():
    request_body='''{"data":"1111指定的返回数据"}'''
    return request_body

@app.route("/", methods=["GET", "POST"])
def html_test():
    return render_template('test.html',txt='Flask常用演示')

if __name__ == '__main__':
    app.run(host='localhost',port='1223',debug=True)